class Progress {
  constructor() {
    this.value = 0;
  }

  increment(incremental = 1) {
    this.value += incremental;
    if (this.value > 100){
      this.value = 100;
    }
  }

  canIncrement() {
    return this.value < 100;
  }
}

describe('練習', () => {
  describe('increment', () => {
    test('進捗は最初は0', () => {
      const target = new Progress();

      expect(target.value).toEqual(0);
    });
    test( '進捗を規定値進める', () => {
      const target = new Progress();

      target.increment();

      expect(target.value).toEqual(1);
    });
    test('進捗を規定値　30　回進める', () => {
      const target = new Progress();

      for (let i = 0; i < 30; i += 1) {
        target.increment();
      }

      expect(target.value).toEqual(30);
    });
    test( '規定値を　20　進める', () => {
      const target = new Progress();

      target.increment(20);

      expect(target.value).toEqual(20);
    });
    test('進捗は　100　を越えない', () => {
      const target = new Progress();

      target.increment(60);
      target.increment(60);

      expect(target.value).toEqual(100);
    });
  });
  describe('canIncrement', () => {
    const target = new Progress();
    target.increment(99);

    expect(target.canIncrement()).toBeTruthy();
  });
});
